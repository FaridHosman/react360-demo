import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Model,
  asset,
  NativeModules,
} from 'react-360';

const {AudioModule} = NativeModules;

AudioModule.playEnvironmental({
  source: asset('nature2.mp3'),
  volume: 0.3,
});

export default class fe_practice extends React.Component {
  render() {
    return (
      <View style={styles.panel}>
        <Model
          source={{obj: asset('assets/logo.obj'), mtl: asset('assets/logo.mtl')}}
          style={{ transform: [{scale: 17}]}}
        />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  panel: {
    // Fill the entire surface
    width: 1000,
    height: 600,
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  greetingBox: {
    padding: 20,
    backgroundColor: '#000000',
    borderColor: '#639dda',
    borderWidth: 2,
  },
  greeting: {
    fontSize: 30,
  },
});

AppRegistry.registerComponent('fe_practice', () => fe_practice);
